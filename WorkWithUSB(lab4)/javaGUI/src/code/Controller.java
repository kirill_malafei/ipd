package code;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.util.LinkedList;

public class Controller {

    @FXML
    TextArea outputTextArea;
    @FXML
    TextArea logTextArea;
    @FXML
    Button ejectButton;
    @FXML
    ChoiceBox devicesChoiceBox;

    @FXML
    public void initialize() {
        outputTextArea.setEditable(false);
        logTextArea.setEditable(false);
        RefreshThread refreshThread = new RefreshThread(outputTextArea, logTextArea, devicesChoiceBox);
        refreshThread.start();
        ejectButton.setOnMouseClicked((MouseEvent e) -> {
            ejectButton.setDisable(true);
            ejectButton.setText("ejecting...");
            DeviceInfo deviceInfo = (DeviceInfo) devicesChoiceBox.getValue();
            if (deviceInfo != null) {
                boolean res = NativeClass.ejectDevice(deviceInfo.getDeviceInst());
                if (res) {
                    logTextArea.appendText("Device " + deviceInfo.getDescription() + " was ejected.\n\r");
                } else {
                    logTextArea.appendText("Can't eject " + deviceInfo.getDescription() + "\n\r");
                }
            }
            ejectButton.setText("eject");
            ejectButton.setDisable(false);
        });
        Main.getPrimaryStage().setOnCloseRequest(event -> {
            refreshThread.setIsRepeat(false);
            try {
                refreshThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Closed");
        });

    }
}
