package code;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;

import javax.sound.sampled.Line;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.nio.file.Paths;
import java.util.LinkedList;

public class RefreshThread extends Thread {

    TextArea outputTextArea;
    TextArea logTextArea;
    ChoiceBox devicesChoiceBox;
    NativeClass nativeClass;
    private boolean isRepeat;

    LinkedList<DeviceInfo> lastDevices;
    LinkedList<DeviceInfo> currentDevices;

    public RefreshThread(TextArea outputTextArea, TextArea logTextArea, ChoiceBox devicesChoiceBox) {
        this.outputTextArea = outputTextArea;
        this.logTextArea = logTextArea;
        this.devicesChoiceBox = devicesChoiceBox;
        nativeClass = new NativeClass();
        isRepeat = true;
    }

    @Override
    public void run() {
        lastDevices = nativeClass.getDeviceList();
        show(lastDevices);
        setChoiceBoxItems(lastDevices);
        while (isRepeat) {
            currentDevices = nativeClass.getDeviceList();
            outputTextArea.clear();
            show(currentDevices);
            if (isDifference(lastDevices, currentDevices)) {
                System.out.println("DIFFFFFFFFFFFFFFEEEEEEEEEEEEEEERENT");
                outputTextArea.clear();
                show(currentDevices);
                setChoiceBoxItems(currentDevices);
                for (int i = 0; i < lastDevices.size(); i++) {
                    if (!currentDevices.contains(lastDevices.get(i))) {
                        logTextArea.appendText("Disconnected: " + lastDevices.get(i) + "\n\r");
                    }
                }
                for (int i = 0; i < currentDevices.size(); i++) {
                    if (!lastDevices.contains(currentDevices.get(i))) {
                        logTextArea.appendText("Connected: " + currentDevices.get(i) + "\n\r");
                    }
                }
                lastDevices = currentDevices;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void show(LinkedList<DeviceInfo> devices) {
        for (int i = 0; i < devices.size(); i++) {
            outputTextArea.appendText(devices.get(i).toString() + "\n\r");
            if (devices.get(i).getDescription().contains("Storage")) {
                outputTextArea.appendText(getMemory());
            }
        }
    }

    private String getMemory() {
        String str = "";
        File[] roots = File.listRoots();
        for (File r : roots) {
            FileSystemView fileSystemView = FileSystemView.getFileSystemView();
            if (!fileSystemView.getSystemTypeDescription(r).contains("Disk")) {
                str = str +
                        "\tTotal space (bytes): " + r.getTotalSpace() + "\n\r" +
                        "\tFree space (bytes): " + r.getFreeSpace() + "\n\r" +
                        "\tUsable space (bytes): " + r.getUsableSpace() + "\n\r";
            }
        }
        return str;
    }

    private void setChoiceBoxItems(LinkedList<DeviceInfo> devices) {
        Platform.runLater(() -> {
            System.out.println("BOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOX");
            devicesChoiceBox.setItems(FXCollections.observableList(devices));
            if (!devices.isEmpty()) devicesChoiceBox.setValue(devices.get(0));
        });
    }

    public boolean isDifference(LinkedList<DeviceInfo> lastDevices, LinkedList<DeviceInfo> currentDevices) {
        if (lastDevices.size() != currentDevices.size()) return true;
        for (int i = 0; i < lastDevices.size(); i++) {
            if (!lastDevices.get(i).equals(currentDevices.get(i)))
                return true;
        }
        return false;
    }

    public void setIsRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }
}
