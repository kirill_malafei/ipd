package code;

public class DeviceInfo {
    private String hardwareID;
    private String objectName;
    private String description;
    private String deviceProducer;
    private String deviceInst;

    public String getHardwareID() {
        return hardwareID;
    }

    public void setHardwareID(String hardwareID) {
        this.hardwareID = hardwareID;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviceProducer() {
        return deviceProducer;
    }

    public void setDeviceProducer(String deviceProducer) {
        this.deviceProducer = deviceProducer;
    }

    public short getDeviceInst() {
        return Short.parseShort(deviceInst);
    }

    public void setDeviceInst(String deviceInst) {
        this.deviceInst = deviceInst;
    }

    @Override
    public String toString() {
        String string = getDescription();
        return string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceInfo that = (DeviceInfo) o;

        if (hardwareID != null ? !hardwareID.equals(that.hardwareID) : that.hardwareID != null) return false;
        if (objectName != null ? !objectName.equals(that.objectName) : that.objectName != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (deviceProducer != null ? !deviceProducer.equals(that.deviceProducer) : that.deviceProducer != null)
            return false;
        return deviceInst != null ? deviceInst.equals(that.deviceInst) : that.deviceInst == null;
    }

    @Override
    public int hashCode() {
        int result = hardwareID != null ? hardwareID.hashCode() : 0;
        result = 31 * result + (objectName != null ? objectName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (deviceProducer != null ? deviceProducer.hashCode() : 0);
        result = 31 * result + (deviceInst != null ? deviceInst.hashCode() : 0);
        return result;
    }
}
