package code;

import java.util.LinkedList;
import java.util.List;

public class NativeClass {

    static {
        System.loadLibrary("library");
    }

    public LinkedList<DeviceInfo> getDeviceList() {
        LinkedList<DeviceInfo> devices = new LinkedList<>();
        String info = getUSBDevices();
        String[] str = info.split(" \\| ");
        System.out.println("Info " + info);
        if (str.length == 1) {
            return new LinkedList<>();
        }
        for (int i = 0; i < str.length; i += 5) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.setDeviceInst(str[i]);
            deviceInfo.setDeviceProducer(str[i + 1]);
            deviceInfo.setHardwareID(str[i + 2]);
            deviceInfo.setObjectName(str[i + 3]);
            deviceInfo.setDescription(str[i + 4]);
            devices.add(deviceInfo);
        }
        return devices;
    }

    public static native boolean ejectDevice(short devInst);

    private native String getUSBDevices();
}
