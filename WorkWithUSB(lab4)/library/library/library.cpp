#include <Windows.h>
#include <cfgmgr32.h>
#include <SetupAPI.h>
#include <vector>
#include <string>
#include <thread>
#include <locale>
#include <Usbioctl.h>
#include <list>
#include <atlstr.h>
#include <Usbiodef.h>
#include <fstream>
#include <sstream>
#include <jni.h>
#include "code_NativeClass.h"
#pragma comment(lib, "setupapi.lib")

const int BUF_SIZE = 512;
using namespace std;

template <typename T>
wstring toString(T val)
{
	ostringstream oss;
	oss << val;
	string str = oss.str();
	return wstring(str.begin(), str.end());
}

JNIEXPORT jstring JNICALL Java_code_NativeClass_getUSBDevices
(JNIEnv * env, jobject jobj) {

	wstring info(L"");
	const GUID* guid = &GUID_DEVINTERFACE_DISK;

	HDEVINFO hDevInfo = SetupDiGetClassDevs(guid, L"USB", NULL, DIGCF_PRESENT | DIGCF_ALLCLASSES);
	DWORD dwIndex = 0;

	SP_DEVINFO_DATA                  spdd;
	spdd.cbSize = sizeof(SP_DEVINFO_DATA);
	
	while (true) {
		if (!SetupDiEnumDeviceInfo(hDevInfo, dwIndex, &spdd))
		{
			break;
		}
		dwIndex++;

		DWORD requiredSize = 0;
		WCHAR *propBuffer = new WCHAR[BUF_SIZE];

		info += toString(spdd.DevInst);
		info += L" | ";

		// Получение имени производителя
		SetupDiGetDeviceRegistryProperty(hDevInfo, &spdd, SPDRP_MFG, 0, (BYTE*)propBuffer, BUF_SIZE, &requiredSize);
		info += propBuffer;
		info += L" | ";
		// Получение hardwareID
		SetupDiGetDeviceRegistryProperty(hDevInfo, &spdd, SPDRP_HARDWAREID, 0, (BYTE*)propBuffer, BUF_SIZE, &requiredSize);
		info += propBuffer;
		info += L" | ";
		// 
		SetupDiGetDeviceRegistryProperty(hDevInfo, &spdd, SPDRP_PHYSICAL_DEVICE_OBJECT_NAME, 0, (BYTE*)propBuffer, BUF_SIZE, &requiredSize);
		info += propBuffer;
		info += L" | ";
		// Получение описания устройства
		SetupDiGetDeviceRegistryProperty(hDevInfo, &spdd, SPDRP_DEVICEDESC, 0, (BYTE*)propBuffer, BUF_SIZE, &requiredSize);
		info += propBuffer;
		info += L" | ";
		delete[] propBuffer;
	}
	SetupDiDestroyDeviceInfoList(hDevInfo);
	jstring result = (*env).NewString((jchar*)info.data(), info.size());
	return result;
}

JNIEXPORT jboolean JNICALL Java_code_NativeClass_ejectDevice
(JNIEnv * env, jobject jobj, jshort di) {
	printf("Starting ejecting device.");

	DEVINST devInst = di;

	PNP_VETO_TYPE VetoType = PNP_VetoTypeUnknown;
	WCHAR VetoNameW[MAX_PATH];
	VetoNameW[0] = 0;
	bool bSuccess = false;

	DEVINST DevInstParent = 0;
	// Получение номера родителя
	DWORD res = CM_Get_Parent(&DevInstParent, devInst,
		0);														// Должно быть 0
	
	for (int tries = 1; tries <= 10; tries++) {

		printf("  Attemp %d...\n", tries);

		VetoType = PNP_VetoTypeUnknown;
		VetoNameW[0] = 0;

		// Попытка извлечь устройство
		res = CM_Request_Device_EjectW(devInst,					// Номер устройства
			&VetoType,											// Будет содержать причину ошибки
			VetoNameW,											// Строка, информация зависит от ошибки VetoType
			MAX_PATH,											// Размер строки
			0);													// Не используется
		bSuccess = (res == CR_SUCCESS && VetoType == PNP_VetoTypeUnknown);
		if (bSuccess) {
			break;
		}

		// Попытка извлечь родителя
		res = CM_Request_Device_EjectW(DevInstParent, &VetoType, VetoNameW, MAX_PATH, 0);
		bSuccess = (res == CR_SUCCESS && VetoType == PNP_VetoTypeUnknown);
		if (bSuccess) {
			break;
		}

		Sleep(100);
	}

	return bSuccess;
}