package code;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    private static final String iconImageLoc = "E:\\study\\IPD\\ipd_private\\WorkWithCD(lab7)\\src\\resources\\GameCenter-icon.png";
    private static final int BUFFER_SIZE = 1000_000;
    private static final String CD_DISK_TYPE = "CD Drive";

    private Stage stage;
    private LinkedList<File> files;
    private FileChooser fileChooser;
    Thread writingThread;
    volatile boolean isRepeat = false;
    java.awt.TrayIcon trayIcon;
    private long diskFreeSpace;
    private boolean canWriteToDisk;
    private String diskPath;

    @FXML
    ListView fielsListView;
    @FXML
    Button openFileButton;
    @FXML
    Button addFileButton;
    @FXML
    TextArea logTextArea;
    @FXML
    Button writeToDiskButton;
    @FXML
    ProgressBar writingProgressBar;
    @FXML
    Button clearListButton;
    @FXML
    Button toTrayButton;

    @FXML
    public void initialize() {
        Platform.setImplicitExit(false);
        stage = Main.getStage();
        files = new LinkedList<>();
        fileChooser = new FileChooser();
        addFileButton.setOnMouseClicked((event) -> {
            fileChooser.setTitle("Choose file to write");
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                files.add(file);
                fielsListView.setItems(FXCollections.observableList(files));
            }
        });
        clearListButton.setOnMouseClicked((event) -> {
            files.clear();
            fielsListView.setItems(FXCollections.observableList(files));
        });
        toTrayButton.setOnMouseClicked((event) -> {
            addAppToTray();
        });
        writeToDiskButton.setOnMouseClicked((event) -> {
            getDiskInfo();
            if (!canWriteToDisk) {
                writeToLog("Can't write to disk.");
                return;
            }
            if (files.isEmpty()) {
                writeToLog("Choose files.");
                return;
            }
            if (isRepeat()) {
                writeToLog("Disk is busy.");
                return;
            }
            long filesSize = 0;
            for (File f : files) {
                filesSize += f.length();
            }
            if (filesSize > diskFreeSpace) {
                writeToLog("Not enough space in disk.");
                return;
            }
            setRepeat(true);
            writingThread = new Thread(() -> {
                for (int i = 0; i < files.size(); i++) {
                    writeFileToDisk(files.get(i));
                }
                setRepeat(false);
            });
            writingThread.start();
        });
        stage.setOnCloseRequest((event) -> {
            setRepeat(false);
            try {
                if (writingThread != null) {
                    writingThread.join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.exit();
        });
    }

    public void getDiskInfo() {
        File[] roots = File.listRoots();
        FileSystemView fileSystemView = FileSystemView.getFileSystemView();

        for (File r : roots) {
            if (fileSystemView.getSystemTypeDescription(r).equals(CD_DISK_TYPE)) {
                diskPath = r.getAbsolutePath();
                canWriteToDisk = r.canWrite();
                System.out.println("Can write " + canWriteToDisk);
                diskFreeSpace = r.getFreeSpace();
                System.out.println("Disk free space: " + diskFreeSpace);
            }
        }
    }

    public synchronized boolean isRepeat() {
        return isRepeat;
    }

    public synchronized void setRepeat(boolean repeat) {
        isRepeat = repeat;
    }

    private void writeToLog(String string) {
        synchronized (logTextArea) {
            logTextArea.appendText(string + "\r\n");
        }
    }

    private void writeFileToDisk(File file) {
        final long fileSize = file.length();
        if (!file.canRead()) {
            writeToLog("can't read file: \"" + file.toString() + "\"");
            return;
        }
        try {
            File diskFile = new File(diskPath + file.getName());
            diskFile.createNewFile();
            if (!diskFile.canWrite()) {
                writeToLog("can't write to disk.");
                return;
            }
            FileOutputStream diskOutputStream = new FileOutputStream(diskFile);
            FileInputStream inputStream = new FileInputStream(file);
            //RandomAccessFile inputStream = new RandomAccessFile(file, "r");
            //inputStream.seek(0);
            byte buffer[] = new byte[BUFFER_SIZE];
            long bytesWereWritten = 0;
            double lastPercent = 0;
            while (isRepeat()) {
                int bytesRead = inputStream.read(buffer);
                System.out.println("Read " + bytesRead);
                if (bytesRead == -1) {
                    break;
                }
                if (bytesRead < BUFFER_SIZE) {
                    byte bufferToWrite[] = new byte[bytesRead];
                    System.arraycopy(buffer, 0, bufferToWrite, 0, bytesRead);
                    diskOutputStream.write(bufferToWrite);
                } else {
                    diskOutputStream.write(buffer);
                }
                diskOutputStream.flush();
                bytesWereWritten += bytesRead;
                double percent = ((double) bytesWereWritten / fileSize);
                System.out.println("Percent " + percent);

                if (stage.isShowing()) {
                    synchronized (writingProgressBar) {
                        writingProgressBar.setProgress(percent);
                    }
                } else {
                    if (percent >= lastPercent + 0.2) {
                        javax.swing.SwingUtilities.invokeLater(() ->
                                trayIcon.displayMessage(
                                        "CD burn",
                                        "Percent " + (int) (percent * 100),
                                        java.awt.TrayIcon.MessageType.INFO
                                )
                        );
                        lastPercent = percent;
                    }
                }
            }
            inputStream.close();
            diskOutputStream.close();
            writeToLog("File was written: " + file.getName());
            if (!stage.isShowing()) {
                javax.swing.SwingUtilities.invokeLater(() ->
                        trayIcon.displayMessage(
                                "CD burn",
                                "Done",
                                java.awt.TrayIcon.MessageType.INFO
                        )
                );
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void showStage() {
        if (stage != null) {
            stage.show();
            stage.toFront();
        }
    }

    private void addAppToTray() {
        try {
            // ensure awt toolkit is initialized.
            java.awt.Toolkit.getDefaultToolkit();

            // app requires system tray support, just exit if there is no support.
            if (!java.awt.SystemTray.isSupported()) {
                System.out.println("No system tray support, application exiting.");
                Platform.exit();
            }

            // set up a system tray icon.
            java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
            URL urlPath = (new File(iconImageLoc)).toURI().toURL();
            java.awt.Image image = ImageIO.read(urlPath);
            trayIcon = new java.awt.TrayIcon(image);

            // if the user double-clicks on the tray icon, show the main app stage.
            trayIcon.addActionListener(event -> Platform.runLater(() -> {
                tray.remove(trayIcon);
                showStage();
            }));

            // if the user selects the default menu item (which includes the app name),
            // show the main app stage.
            java.awt.MenuItem openItem = new java.awt.MenuItem("CD burn");
            openItem.addActionListener(event -> Platform.runLater(() -> {
                tray.remove(trayIcon);
                showStage();
            }));
            // the convention for tray icons seems to be to set the default icon for opening
            // the application stage in a bold font.
            java.awt.Font defaultFont = java.awt.Font.decode(null);
            java.awt.Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
            openItem.setFont(boldFont);

            // to really exit the application, the user must go to the system tray icon
            // and select the exit option, this will shutdown JavaFX and remove the
            // tray icon (removing the tray icon will also shut down AWT).
            java.awt.MenuItem exitItem = new java.awt.MenuItem("Exit");
            exitItem.addActionListener(event -> {
                tray.remove(trayIcon);
                setRepeat(false);
                if (writingThread != null) {
                    try {
                        writingThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Platform.exit();
            });

            // setup the popup menu for the application.
            final java.awt.PopupMenu popup = new java.awt.PopupMenu();
            popup.add(openItem);
            popup.addSeparator();
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);

            tray.add(trayIcon);
            stage.hide();
        } catch (java.awt.AWTException | IOException e) {
            System.out.println("Unable to init system tray");
            e.printStackTrace();
        }
    }
}
