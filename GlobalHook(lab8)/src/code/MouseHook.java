package code;

import lc.kra.system.mouse.GlobalMouseHook;
import lc.kra.system.mouse.event.GlobalMouseEvent;
import lc.kra.system.mouse.event.GlobalMouseListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MouseHook {
    private GlobalMouseHook globalMouseHook;
    private GlobalMouseListener globalMouseListener;
    private LogWriter logWriter;

    public MouseHook(LogWriter logWriter) {
        this.logWriter = logWriter;
        globalMouseHook = new GlobalMouseHook();
        globalMouseListener = new GlobalMouseListener() {
            @Override
            public void mousePressed(GlobalMouseEvent globalMouseEvent) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                logWriter.writeMouseEvent(dateFormat.format(date) + ": " +
                        globalMouseEvent.toString());
            }

            @Override
            public void mouseReleased(GlobalMouseEvent globalMouseEvent) {
                //logWriter.writeMouseEvent(globalMouseEvent.toString());
            }

            @Override
            public void mouseMoved(GlobalMouseEvent globalMouseEvent) {
                //logWriter.writeMouseEvent(globalMouseEvent.toString());
            }

            @Override
            public void mouseWheel(GlobalMouseEvent globalMouseEvent) {
                //logWriter.writeMouseEvent(globalMouseEvent.toString());
            }
        };
    }

    public void startMouseHook() {
        if (isWindows()) {
            new Thread(() -> {
                globalMouseHook.addMouseListener(globalMouseListener);
            }).start();
        }
    }

    public void stopMouseHook() {
        if (isWindows() && globalMouseHook != null) {
            globalMouseHook.removeMouseListener(globalMouseListener);
        }
    }

    public boolean isWindows() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("win") >= 0);
    }
}
