package code;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinDef.LRESULT;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.HHOOK;

import com.sun.jna.platform.win32.WinUser.LowLevelKeyboardProc;
import com.sun.jna.platform.win32.WinUser.MSG;
import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import lc.kra.system.keyboard.event.GlobalKeyListener;


public class KeyHook {
    private static final int WH_KEYBOARD_LL = 13;
    private final int TERMINATE_THREAD = -1;

    private HHOOK hhk;
    private LowLevelKeyboardProc keyboardHook;
    private User32 lib;
    LogWriter logWriter;
    Thread thread;
    MSG msg;

    public KeyHook(LogWriter logWriter) {
        this.logWriter = logWriter;
    }

    public void startKeyHook() {

        if (isWindows()) {
            thread = new Thread(new Runnable() {
                private final int TAB = 9;
                private final int SPACE = 32;
                //private final int

                @Override
                public void run() {
                    lib = User32.INSTANCE;
                    HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
                    keyboardHook = new LowLevelKeyboardProc() {
                        public LRESULT callback(int nCode, WPARAM wParam, WinUser.KBDLLHOOKSTRUCT info) {
                            System.out.println("second hook: " + info.vkCode);
                            if (nCode >= 0) {
                                System.out.println("wParam: " + wParam);

                                //logWriter.writeKeyEvent(info.vkCode);
                                //System.out.println("Code " + info.vkCode);
                                //System.out.println("SYSKEYDOWN: " + (wParam.intValue() & WinUser.WM_SYSKEYDOWN));
                                switch (info.vkCode) {
                                    case SPACE:
                                        if ((wParam.intValue() & WinUser.WM_SYSKEYDOWN) == WinUser.WM_SYSKEYDOWN) {
                                            System.out.println("SHOW STAGE");
                                            Main.showStage();
                                            return new LRESULT(0);
                                        }
                                        break;
                                    case TAB:
                                        if ((wParam.intValue() & WinUser.WM_SYSKEYDOWN) == WinUser.WM_SYSKEYDOWN) {
                                            return new LRESULT(1);
                                        }
                                        break;
                                    default:

                                }
                            }
                            return new LRESULT(0);
                            //return lib.CallNextHookEx(hhk, nCode, wParam, new WinDef.LPARAM());
                        }
                    };
                    hhk = lib.SetWindowsHookEx(WH_KEYBOARD_LL, keyboardHook, hMod, 0);

                    int result;
                    /*MSG msg = new MSG();
                    while ((result = lib.GetMessage(msg, null, 0, 0)) != 0) {
                        if (result == TERMINATE_THREAD) {
                            break;
                        } else {
                            System.out.println("DEBUG fuck: " + msg.toString(true));
                            lib.TranslateMessage(msg);
                            lib.DispatchMessage(msg);
                        }
                    }
                    */

                    msg = new MSG();
                    lib.PeekMessage(msg,null,0,10,0);
                    System.out.println("Thread id from thread: " + thread.getId());
                    result = lib.GetMessage(msg, null, 0, 5);
                    System.out.println("Error in GetMessage" + Native.getLastError());
                    System.out.println("result " + result);
                    if (result == TERMINATE_THREAD) {
                        System.out.println("Key hook thread terminated.");
                        lib.UnhookWindowsHookEx(hhk);
                    }

                }
            }, "as");

            thread.start();
        }
    }

    void stopKeyHook() {
        if (isWindows() && lib != null) {
            lib.UnhookWindowsHookEx(hhk);
        }
        synchronized (lib) {
            WPARAM wparam = new WPARAM();
            System.out.println("Thread id: " + (int)thread.getId());
            int res = 0;
            res = lib.PostThreadMessage((int)thread.getId(), 1, null, null);
            System.out.println(Native.getLastError());
            System.out.println();
            System.out.println("PostThreadMessage result: " + res);
            //thread.join();
        }

    }

    public boolean isWindows() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("win") >= 0);
    }
}