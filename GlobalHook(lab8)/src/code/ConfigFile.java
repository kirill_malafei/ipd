package code;

import javax.print.attribute.standard.Destination;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigFile {
    private static final int CODE_NUMBER = 12;
    private static final String CONFIGURATION_FILE = "C:\\Users\\Kimentii\\Desktop\\Log\\config_file.txt";
    private static final String DESTINATION_EMAIL_STR = "destination email: ";
    private static final String MAX_CONFIG_FILE_STR = "max log file length: ";
    private static final String IS_SILENT_START_STR = "is silent start: ";
    private static final String DEFAULT_DESTINATION_MAIL = "malafeikirill@mail.ru";
    private static final int DEFAULT_MAX_CONFIG_FILE_SIZE = 500;
    private static final boolean DEFAULT_IS_SILENT_MODE = false;

    private RandomAccessFile configurationFile;
    private String destinationMail;
    private int maxLogFileSize;
    private boolean isSilentStart;
    private static ConfigFile instance;

    private ConfigFile() {
        File file = new File(CONFIGURATION_FILE);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            configurationFile = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ConfigFile getInstance() {
        if (instance == null) {
            instance = new ConfigFile();
        }
        return instance;
    }

    private String encode(String data) {
        byte dataBytes[] = data.getBytes();
        for (int i = 0; i < dataBytes.length; i++) {
            dataBytes[i] -= CODE_NUMBER;
        }
        return new String(dataBytes);
    }

    private String decode(String code) {
        byte codeBytes[] = code.getBytes();
        for (int i = 0; i < codeBytes.length; i++) {
            codeBytes[i] += CODE_NUMBER;
        }
        return new String(codeBytes);
    }

    private String decode(byte[] codeBytes) {
        for (int i = 0; i < codeBytes.length; i++) {
            codeBytes[i] += CODE_NUMBER;
        }
        return new String(codeBytes);
    }

    public boolean parseInfo(String info) {
        Pattern pattern = Pattern.compile("(" + DESTINATION_EMAIL_STR + ")([0-9a-zA-Z@.]{1,255})");
        Matcher matcher = pattern.matcher(info);
        if (matcher.find()) {
            destinationMail = matcher.group(2);
        } else {
            System.out.println("Error in email pursing.");
            destinationMail = DEFAULT_DESTINATION_MAIL;
            return false;
        }
        pattern = Pattern.compile("(" + MAX_CONFIG_FILE_STR + ")([0-9]{1,10})");
        matcher = pattern.matcher(info);
        if (matcher.find()) {
            maxLogFileSize = Integer.parseInt(matcher.group(2));
        } else {
            System.out.println("Error in log file length pursing.");
            maxLogFileSize = DEFAULT_MAX_CONFIG_FILE_SIZE;
            return false;
        }
        pattern = Pattern.compile("(" + IS_SILENT_START_STR + ")([0-1])");
        matcher = pattern.matcher(info);
        if (matcher.find()) {
            isSilentStart = Boolean.parseBoolean(matcher.group(2));
        } else {
            System.out.println("Error in silent start boolean pursing.");
            isSilentStart = DEFAULT_IS_SILENT_MODE;
            return false;
        }
        return true;
    }

    public String readConfigurations() {
        String info = "";
        try {
            if (configurationFile.length() == 0) {
                writeConfigurations(DEFAULT_DESTINATION_MAIL, DEFAULT_MAX_CONFIG_FILE_SIZE, DEFAULT_IS_SILENT_MODE);
            }
            byte data[] = new byte[(int) configurationFile.length()];
            configurationFile.seek(0);
            configurationFile.read(data);
            info = decode(data);
            System.out.println("Read info from file: " + info);
            parseInfo(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return info;
    }

    public boolean writeConfigurations(String config) {
        try {
            configurationFile.seek(0);
            configurationFile.write(encode(config).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean writeConfigurations(String destinationMail, int maxLogFileSize, boolean isSilentStart) {
        String data = DESTINATION_EMAIL_STR + destinationMail + " "
                + MAX_CONFIG_FILE_STR + maxLogFileSize + " "
                + IS_SILENT_START_STR + (isSilentStart ? "1" : "0") + " ";
        try {
            System.out.println("Writing info: " + data);
            configurationFile.write(encode(data).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String getDestinationMail() {
        return destinationMail;
    }

    public void setDestinationMail(String destinationMail) {
        this.destinationMail = destinationMail;
    }

    public int getMaxLogFileSize() {
        return maxLogFileSize;
    }

    public void setMaxLogFileSize(int maxLogFileSize) {
        this.maxLogFileSize = maxLogFileSize;
    }

    public boolean isSilentStart() {
        return isSilentStart;
    }

    public void setSilentStart(boolean silentStart) {
        isSilentStart = silentStart;
    }
}
