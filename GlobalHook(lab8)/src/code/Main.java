package code;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage stage;


    public static void showStage() {
        Platform.runLater(() -> {
            synchronized (stage) {
                stage.show();
            }
        });
    }

    public static void hideStage() {
        synchronized (stage) {
            stage.hide();
        }
    }

    public static void setStageOnCloseRequest(javafx.event.EventHandler<javafx.stage.WindowEvent> value) {
        synchronized (stage) {
            stage.setOnCloseRequest(value);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Main.stage = primaryStage;
        Platform.setImplicitExit(false);
        Parent root = FXMLLoader.load(getClass().getResource("../resources/main_menu.fxml"));
        primaryStage.setTitle("Global hook");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private static boolean run = true;

    public static void main(String[] args) {
        launch(args);
    }

}
