#pragma comment (lib, "Setupapi.lib")
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <setupapi.h>
#include <devguid.h>
#include <regstr.h>
#include <iostream>
#include <conio.h>
#include <iterator>
#include <algorithm>
#include <string> 
#include <dbt.h>
#include <exception>
#include <string>
#include <jni.h>
#include "code_NativeClass.h"
#pragma warning(disable:4996);

using namespace std;

const int BUFFER_SIZE = 1024;
const wstring DIVIDER(L" | ");

// возвращает 2 значения
wstring GetDriverName(const wchar_t *szServiceName)
{
	HKEY  hKey = 0L;
	wchar_t  szSubKey[128] = L"SYSTEM\\CurrentControlSet\\Services\\\0";
	wchar_t  szPath[MAX_PATH] = { 0 };
	DWORD cbData;
	DWORD dwType;
	DWORD dwStart;
	wstring result(L"");
	
	wcscat(szSubKey, szServiceName);
	long int ans = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		(LPCWSTR)szSubKey,
		0,
		KEY_ALL_ACCESS,
		&hKey);
	if (ans != ERROR_SUCCESS){
		return wstring(DIVIDER + DIVIDER);
	}
	
	cbData = MAX_PATH - 1;
	dwType = REG_EXPAND_SZ;
	wchar_t ip[] = L"ImagePath";
	if (RegQueryValueEx(hKey, (LPCWSTR)ip, 0L,
		&dwType,
		(unsigned char*)szPath,
		&cbData) != ERROR_SUCCESS) {
		result += DIVIDER;
		// printf("Image path: N/A\n");
	}
	else
	{
		wchar_t szRoot[MAX_PATH] = { 0 };
		GetWindowsDirectory((LPWSTR)szRoot, MAX_PATH - 1);
		//wcscat(szRoot, L"\\");
		wcscat(szRoot, szPath);
		result += szRoot;
		result += DIVIDER;
		// printf("Image path: %ws\n", szRoot);
	};
	//
	RtlZeroMemory(szPath, MAX_PATH);
	cbData = MAX_PATH - 1;
	dwType = REG_SZ;
	wchar_t group[] = L"Group";
	if (RegQueryValueEx(hKey, (LPCWSTR)group, 0L,
		&dwType,
		(unsigned char*)szPath,
		&cbData) != ERROR_SUCCESS)
	{
		result += DIVIDER;
		// printf("Group: N/A\n");
	}
	else
	{
		result += szPath;
		result += DIVIDER;
		// printf("Group: %ws\n", szPath);
	}
	RegCloseKey(hKey);
	return result;
};

// Возвращает 2 значения
wstring GetOtherInfo(HDEVINFO hDevInfo, SP_DEVINFO_DATA spDevInfoData, short wOrder)
{
	if (hDevInfo == (void*)-1)
	{
		// printf("ERROR: %l\n", GetLastError());
		return wstring(L"");
	};
	
		SP_DRVINFO_DATA        spDrvInfoData = { 0 };
		SP_DRVINFO_DETAIL_DATA spDrvInfoDetail = { 0 };
		char					szHardware[128] = { 0 };
		DWORD                  dwRequireSize;
		short                  wIdx;
		wstring					result(L"");
		if (!SetupDiBuildDriverInfoList(hDevInfo,
			&spDevInfoData,
			SPDIT_COMPATDRIVER))
			return wstring(DIVIDER + DIVIDER);
			// // printf("Error1\n");
		
		wIdx = 0;
		//while (1)
		{
			spDrvInfoData.cbSize = sizeof(SP_DRVINFO_DATA);
			if (SetupDiEnumDriverInfo(hDevInfo,
				&spDevInfoData,
				SPDIT_COMPATDRIVER,
				wIdx++,
				&spDrvInfoData))
			{
				dwRequireSize = 0;
				if (!SetupDiGetDriverInfoDetail(hDevInfo,
					&spDevInfoData,
					&spDrvInfoData,
					&spDrvInfoDetail,
					sizeof(SP_DRVINFO_DETAIL_DATA),
					&dwRequireSize) &&
					GetLastError() != ERROR_INSUFFICIENT_BUFFER)
				{
					SYSTEMTIME sysTime = { 0 };
					wchar_t    szTmp[64] = { 0 };
					//
					memcpy(szHardware, spDrvInfoDetail.HardwareID,
						wcslen(spDrvInfoDetail.HardwareID));
					result += spDrvInfoData.ProviderName;
					result += DIVIDER;
					result += spDrvInfoData.Description;
					result += DIVIDER;
					// // printf("Provider: %ws\n", spDrvInfoData.ProviderName);
					// // printf("Driver description: %ws\n", spDrvInfoData.Description);
				}
				else {
					return wstring(DIVIDER + DIVIDER);
				}
			}
			else
			{
				return wstring(DIVIDER + DIVIDER);
			}
			SetupDiDestroyDriverInfoList(hDevInfo, &spDevInfoData, SPDIT_COMPATDRIVER);
		}
		return result;
	
};

// Возвращает 1
wstring GetDeviceInterfaceInfo(HDEVINFO hDevInfo, SP_DEVINFO_DATA spDevInfoData)
{
	SP_DEVICE_INTERFACE_DATA spDevInterfaceData = { 0 };
	wstring result(L"");
	//
	spDevInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	if (!SetupDiCreateDeviceInterface(hDevInfo,
		&spDevInfoData,
		&spDevInfoData.ClassGuid,
		0L,
		0L,
		&spDevInterfaceData))
		return wstring(DIVIDER);
		// printf("Error: %d, %s", GetLastError(), "SetupDiBuildDriverInfoList");
	else
	{
		SP_DEVICE_INTERFACE_DETAIL_DATA *pspDevInterfaceDetail = 0L;
		DWORD                           dwRequire = 0L;
		//
		if (!SetupDiGetDeviceInterfaceDetail(hDevInfo,
			&spDevInterfaceData,
			0L,
			0,
			&dwRequire,
			0L))
		{
			DWORD dwError = GetLastError();
			//
			if (dwError != ERROR_INSUFFICIENT_BUFFER)
			{
				// printf("Error: %d, %s", GetLastError(), "SetupDiBuildDriverInfoList");
				return wstring(DIVIDER);
			};
		};
		//
		pspDevInterfaceDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA*)LocalAlloc(LPTR,
			sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA)*dwRequire);
		pspDevInterfaceDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
		if (!SetupDiGetDeviceInterfaceDetail(hDevInfo,
			&spDevInterfaceData,
			pspDevInterfaceDetail,
			dwRequire,
			&dwRequire,
			0L))
		{
			DWORD dwError = GetLastError();
		}
		else
		{
			//// printf("Device path2: %ws\n", pspDevInterfaceDetail->DevicePath);
			result += pspDevInterfaceDetail->DevicePath;   
			result += DIVIDER;
		}
		//
		if (pspDevInterfaceDetail)
			LocalFree(pspDevInterfaceDetail);
	}
	return result;
};

// GUID | HardwareID | Manufacturre | SPDRP_DRIVER | Friendly name | Device Path | Service Name | Provider | Driver desc | Image Path | Group |

JNIEXPORT jstring JNICALL Java_code_NativeClass_getUSBDevices
(JNIEnv* env, jobject obj)
{
	HDEVINFO hDevInfo;								// Дескриптор набора информации об устройстве
													// Получение дескриптора информации об устройсве
	hDevInfo = SetupDiGetClassDevs(
		NULL,										// Указатель на класс установки устройства
		NULL,										// Счетчик PnP
		0,											// Дескриптор интерфейса
		DIGCF_PRESENT |								// Только устройства, присутсвующие в системе
		DIGCF_ALLCLASSES);							// Возвратить все устройства

	if (hDevInfo == INVALID_HANDLE_VALUE)
	{
		// printf("Error with getting info about devices.");
		_getch();
	}

	SP_DEVINFO_DATA deviceInfoData;						// Структура с информацией об устройстве
	deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);	// Установка размера самой структуры.
	int i = 0;
	wchar_t buffer[BUFFER_SIZE];
	wstring info(L"");
	while (true) {
		if (!SetupDiEnumDeviceInfo(hDevInfo, i, &deviceInfoData)) {
			break;
		}
		i++;
		memset(buffer, 0, BUFFER_SIZE * sizeof(wchar_t));

		// Получение GUID
		SetupDiGetDeviceRegistryProperty(
			hDevInfo,									//Информация о наборе устройств
			&deviceInfoData,							//Информация о том, какое утройство из набора требуется
			SPDRP_CLASSGUID,							//Требуемое свойство
			NULL,
			(PBYTE)buffer,								//Буфер
			BUFFER_SIZE,								//Действительный размер буфера
			NULL);								//Требуемый размер буфера
		// printf("GUID: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		memset(buffer, 0, BUFFER_SIZE);
		// Получение HardwareID
		SetupDiGetDeviceRegistryProperty(hDevInfo,
			&deviceInfoData,
			SPDRP_HARDWAREID,
			0L,
			(PBYTE)buffer,
			BUFFER_SIZE,
			0);
		// printf("HardwareID: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		memset(buffer, 0, BUFFER_SIZE);
		// Получение имения производителя
		SetupDiGetDeviceRegistryProperty(hDevInfo,
			&deviceInfoData,
			SPDRP_MFG,
			0L,
			(PBYTE)buffer,
			BUFFER_SIZE,
			0);
		// printf("Manufacture: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		memset(buffer, 0, BUFFER_SIZE);
		// Получение 
		SetupDiGetDeviceRegistryProperty(hDevInfo,
			&deviceInfoData,
			SPDRP_DRIVER,
			0L,
			(PBYTE)buffer,
			BUFFER_SIZE,
			0);
		// printf("SPDRP_DRIVER: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		memset(buffer, 0, BUFFER_SIZE);
		// Получение дружественного имени
		SetupDiGetDeviceRegistryProperty(hDevInfo,
			&deviceInfoData,
			SPDRP_FRIENDLYNAME,
			0L,
			(PBYTE)buffer,
			BUFFER_SIZE,
			0);
		// printf("Friendly name: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		// Поулчение device path
		info += GetDeviceInterfaceInfo(hDevInfo, deviceInfoData);
		// printf("\nDevice Path: %ws\n", r.data());

		memset(buffer, 0, BUFFER_SIZE);
		
		SetupDiGetDeviceRegistryProperty(hDevInfo,
			&deviceInfoData,
			SPDRP_SERVICE,
			0L,
			(PBYTE)buffer,
			BUFFER_SIZE,
			0);
		// printf("Service Name: %ws\n", buffer);
		info += buffer;
		info += DIVIDER;

		info += GetOtherInfo(hDevInfo, deviceInfoData, i);

		info += GetDriverName(buffer);
	}
	SetupDiDestroyDeviceInfoList(hDevInfo);
	jstring result = (*env).NewString((jchar*)info.data(), info.size());
    return result;
}

