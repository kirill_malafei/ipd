package code;

// GUID | HardwareID | Manufacture | driver key | Friendly name |
// Device Path | Service Name | Provider | Driver desc | Image Path | Group |

public class DeviceInfo {
    private String guid;
    private String hardwareID;
    private String manufacture;
    private String driverKey;
    private String friendlyName;
    private String devicePath;
    private String serviceName;
    private String provider;
    private String driverDescription;
    private String pathToSYS;
    private String group;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getHardwareID() {
        return hardwareID;
    }

    public void setHardwareID(String hardwareID) {
        this.hardwareID = hardwareID;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getDriverKey() {
        return driverKey;
    }

    public void setDriverKey(String driverKey) {
        this.driverKey = driverKey;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getDevicePath() {
        return devicePath;
    }

    public void setDevicePath(String devicePath) {
        this.devicePath = devicePath;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDriverDescription() {
        return driverDescription;
    }

    public void setDriverDescription(String driverDescription) {
        this.driverDescription = driverDescription;
    }

    public String getPathToSYS() {
        return pathToSYS;
    }

    public void setPathToSYS(String pathToSYS) {
        this.pathToSYS = pathToSYS;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        if (friendlyName != null && !friendlyName.isEmpty()) {
            return getFriendlyName();
        } else if (getDriverDescription() != null && !getDriverDescription().isEmpty()) {
            return getDriverDescription();
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceInfo that = (DeviceInfo) o;

        if (guid != null ? !guid.equals(that.guid) : that.guid != null) return false;
        if (hardwareID != null ? !hardwareID.equals(that.hardwareID) : that.hardwareID != null) return false;
        if (manufacture != null ? !manufacture.equals(that.manufacture) : that.manufacture != null) return false;
        if (driverKey != null ? !driverKey.equals(that.driverKey) : that.driverKey != null) return false;
        if (friendlyName != null ? !friendlyName.equals(that.friendlyName) : that.friendlyName != null) return false;
        if (devicePath != null ? !devicePath.equals(that.devicePath) : that.devicePath != null) return false;
        if (serviceName != null ? !serviceName.equals(that.serviceName) : that.serviceName != null) return false;
        if (provider != null ? !provider.equals(that.provider) : that.provider != null) return false;
        if (driverDescription != null ? !driverDescription.equals(that.driverDescription) : that.driverDescription != null)
            return false;
        if (pathToSYS != null ? !pathToSYS.equals(that.pathToSYS) : that.pathToSYS != null) return false;
        return group != null ? group.equals(that.group) : that.group == null;
    }

    @Override
    public int hashCode() {
        int result = guid != null ? guid.hashCode() : 0;
        result = 31 * result + (hardwareID != null ? hardwareID.hashCode() : 0);
        result = 31 * result + (manufacture != null ? manufacture.hashCode() : 0);
        result = 31 * result + (driverKey != null ? driverKey.hashCode() : 0);
        result = 31 * result + (friendlyName != null ? friendlyName.hashCode() : 0);
        result = 31 * result + (devicePath != null ? devicePath.hashCode() : 0);
        result = 31 * result + (serviceName != null ? serviceName.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        result = 31 * result + (driverDescription != null ? driverDescription.hashCode() : 0);
        result = 31 * result + (pathToSYS != null ? pathToSYS.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }
}
