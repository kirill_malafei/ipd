package code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class DevicesController {
    public static final String DEVCON_PATH = "C:\\Program Files (x86)\\Windows Kits\\10\\Tools\\x64\\devcon.exe";

    private NativeClass nativeClass;
    private final int DEVICE_INFO_SIZE = 11;

    DevicesController() {
        nativeClass = new NativeClass();
    }

    // GUID | HardwareID | Manufacturre | driver key | Friendly name |
    // Device Path | Service Name | Provider | Driver desc | path to sys | Group |
    public LinkedList<DeviceInfo> getDeviceList() {
        LinkedList<DeviceInfo> devices = new LinkedList<>();
        String info = nativeClass.getUSBDevices();
        String[] strings = info.split(" \\| ");
        //System.out.println("Info " + info);
        if (strings.length % 11 == 0) {
            System.out.println("GOOOOOOOOOOOOOOOOOOOOOOOOD");
        }
        System.out.println("Refresh");
        for (int i = 0; i < strings.length; i += DEVICE_INFO_SIZE) {
            DeviceInfo deviceInfo = new DeviceInfo();
            try {
                deviceInfo.setGuid(strings[i]);
                deviceInfo.setHardwareID(strings[i + 1]);
                deviceInfo.setManufacture(strings[i + 2]);
                deviceInfo.setDriverKey(strings[i + 3]);
                deviceInfo.setFriendlyName(strings[i + 4]);
                deviceInfo.setDevicePath(strings[i + 5]);
                deviceInfo.setServiceName(strings[i + 6]);
                deviceInfo.setProvider(strings[i + 7]);
                deviceInfo.setDriverDescription(strings[i + 8]);
                deviceInfo.setPathToSYS(strings[i + 9]);
                deviceInfo.setGroup(strings[i + 10]);
                if ((!deviceInfo.getDriverDescription().isEmpty() || !deviceInfo.getFriendlyName().isEmpty()) &&
                        !deviceInfo.getHardwareID().isEmpty()) {
                    devices.add(deviceInfo);
                }
                /*System.out.println("-------------------------------------------------------------------");
                System.out.println("Device:\n");
                System.out.println(deviceInfo.getGuid());
                System.out.println(deviceInfo.getHardwareID());
                System.out.println(deviceInfo.getManufacture());
                System.out.println(deviceInfo.getDriverKey());
                System.out.println(deviceInfo.getFriendlyName());
                System.out.println(deviceInfo.getDevicePath());
                System.out.println(deviceInfo.getServiceName());
                System.out.println(deviceInfo.getProvider());
                System.out.println(deviceInfo.getDriverDescription());
                System.out.println(deviceInfo.getPathToSYS());
                System.out.println(deviceInfo.getGroup());
                System.out.println("-----------------------------------------------------------------");
                */
            } catch (ArrayIndexOutOfBoundsException e) {
                break;
            }
        }
        return devices;
    }

    public String disableDevice(DeviceInfo deviceInfo) {
        ProcessBuilder builder = new ProcessBuilder(
                DEVCON_PATH,
                "disable", "\"" + deviceInfo.getHardwareID() + "\"");
        builder.redirectErrorStream(true);          // потоки output и errorOutput совпадают
        String line = "";
        try {
            Process process = builder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }

    public String enableDevice(DeviceInfo deviceInfo) {
        ProcessBuilder builder = new ProcessBuilder(
                DEVCON_PATH,
                "enable", "\"" + deviceInfo.getHardwareID() + "\"");
        builder.redirectErrorStream(true);          // потоки output и errorOutput совпадают
        String line = "";
        try {
            Process process = builder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            line = reader.readLine();
            //System.out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
}
