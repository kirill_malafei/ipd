package code;

public class NativeClass {

    static {
        System.loadLibrary("cppLib");
    }

    public native String getUSBDevices();
}
