package code;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import java.beans.EventHandler;

public class Controller {

    DevicesController devicesController;

    @FXML
    private ListView devicesListView;
    @FXML
    private TextArea outputTextArea;
    @FXML
    private TextArea logTextArea;
    @FXML
    private Button disableButton;
    @FXML
    private Button enableButton;

    @FXML
    public void initialize() {
        outputTextArea.setEditable(false);
        logTextArea.setEditable(false);
        devicesController = new DevicesController();
        //devicesListView.setItems(FXCollections.observableList(devicesController.getDeviceList()));
        devicesListView.setOnMouseClicked((event) -> {
            synchronized (devicesController) {
                DeviceInfo deviceInfo = (DeviceInfo) devicesListView.getSelectionModel().getSelectedItem();
                outputTextArea.clear();
                if (deviceInfo != null) {
                    outputTextArea.appendText("GUID: " + deviceInfo.getGuid() + "\n\r");
                    outputTextArea.appendText("Hardware ID: " + deviceInfo.getHardwareID() + "\n\r");
                    outputTextArea.appendText("Manufacture: " + deviceInfo.getManufacture() + "\n\r");
                    outputTextArea.appendText("Provider: " + deviceInfo.getProvider() + "\n\r");
                    outputTextArea.appendText("Driver description: " + deviceInfo.getDriverDescription() + "\n\r");
                    outputTextArea.appendText("Path to .sys file: " + deviceInfo.getPathToSYS() + "\n\r");
                    outputTextArea.appendText("Device path: " + deviceInfo.getDevicePath() + "\n\r");
                }
            }
        });
        disableButton.setOnMouseClicked((event) -> {
            synchronized (devicesListView) {
                DeviceInfo deviceInfo = (DeviceInfo) devicesListView.getSelectionModel().getSelectedItem();
                if (deviceInfo != null) {
                    String answer = devicesController.disableDevice(deviceInfo);
                    logTextArea.appendText(answer + "\n\r");
                }
            }
        });
        enableButton.setOnMouseClicked(event -> {
            synchronized (devicesListView) {
                DeviceInfo deviceInfo = (DeviceInfo) devicesListView.getSelectionModel().getSelectedItem();
                if (deviceInfo != null) {
                    String answer = devicesController.enableDevice(deviceInfo);
                    logTextArea.appendText(answer + "\n\r");
                }
            }
        });
        RefreshThread refreshThread = new RefreshThread(devicesListView);
        refreshThread.start();
        Main.getPrimaryStage().setOnCloseRequest(event -> {
            refreshThread.setIsRepeat(false);
            try {
                refreshThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Closed");
        });
    }

}
