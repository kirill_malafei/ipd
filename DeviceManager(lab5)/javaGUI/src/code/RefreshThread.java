package code;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

import java.util.LinkedList;

public class RefreshThread extends Thread {
    private ListView devicesListView;
    private DevicesController devicesController;
    private boolean isRepeat;
    LinkedList<DeviceInfo> lastDevices;
    LinkedList<DeviceInfo> currentDevices;

    RefreshThread(ListView devicesListView) {
        this.devicesListView = devicesListView;
        devicesController = new DevicesController();
    }


    @Override
    public void run() {
        lastDevices = devicesController.getDeviceList();
        show(lastDevices);
        isRepeat = true;
        while (isRepeat) {
            currentDevices = devicesController.getDeviceList();
            //outputTextArea.clear();
            //show(currentDevices);
            if (isDifference(lastDevices, currentDevices)) {
                System.out.println("DIFFFFFFFFFFFFFFEEEEEEEEEEEEEEERENT");
                //outputTextArea.clear();
                show(currentDevices);
                /*for (int i = 0; i < lastDevices.size(); i++) {
                    if (!currentDevices.contains(lastDevices.get(i))) {
                        logTextArea.appendText("Disconnected: " + lastDevices.get(i) + "\n\r");
                    }
                }
                for (int i = 0; i < currentDevices.size(); i++) {
                    if (!lastDevices.contains(currentDevices.get(i))) {
                        logTextArea.appendText("Connected: " + currentDevices.get(i) + "\n\r");
                    }
                }*/
                lastDevices = currentDevices;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void show(LinkedList<DeviceInfo> devices) {
        synchronized (devicesListView) {
            Platform.runLater(
                    () -> {
                        devicesListView.setItems(FXCollections.observableList(devices));
                    }
            );
        }
    }

    public boolean isDifference(LinkedList<DeviceInfo> lastDevices, LinkedList<DeviceInfo> currentDevices) {
        if (lastDevices.size() != currentDevices.size()) return true;
        for (int i = 0; i < lastDevices.size(); i++) {
            if (!lastDevices.get(i).equals(currentDevices.get(i)))
                return true;
        }
        return false;
    }

    public void setIsRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }
}
