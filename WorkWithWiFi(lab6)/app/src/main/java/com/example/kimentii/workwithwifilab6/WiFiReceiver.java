package com.example.kimentii.workwithwifilab6;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.widget.TextView;

public class WiFiReceiver extends BroadcastReceiver {

    private String TAG = "TAG";
    private Handler mHandler;
    private TextView mTextView;
    private Element mElement;

    public WiFiReceiver() {
    }

    public WiFiReceiver(Handler handler, Element element, TextView textView) {
        mHandler = handler;
        mTextView = textView;
        mElement = element;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {

        //ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        if (mHandler == null || mElement == null || mTextView == null) {
            return;
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                String wifiName = getWifiName(context);
                if (wifiName == null) {
                    mTextView.setText("Not connected to WiFi network");
                } else if (wifiName.equals(mElement.getSsid())) {
                    mTextView.setText("Connected to this network");
                } else {
                    mTextView.setText("Not connected to this network");
                }
            }
        });
    }

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    if (wifiInfo.getSSID().length() >= 2) {
                        return wifiInfo.getSSID().substring(1, wifiInfo.getSSID().length() - 1);
                    } else {
                        return wifiInfo.getSSID();
                    }
                }
            }
        }
        return null;
    }
}