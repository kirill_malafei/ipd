package com.example.kimentii.workwithwifilab6;

import android.os.Handler;
import android.os.Message;

public class Refresher extends Thread {
    private static int DELAY_TIME = 5000;
    private boolean isRepeat;
    Handler mHandler;

    Refresher(Handler handler) {
        this.mHandler = handler;
    }

    public void setIsRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }

    @Override
    public void run() {
        isRepeat = true;
        while (isRepeat) {
            try {
                Thread.sleep(DELAY_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
            Message readMsg = mHandler.obtainMessage();
            readMsg.sendToTarget();
        }
    }
}
