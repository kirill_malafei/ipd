package com.example.kimentii.workwithwifilab6;

import java.io.IOException;
import java.io.Serializable;

public class Element implements Serializable {
    private String ssid;
    private String security;
    private String level;
    private String macAddress;

    public Element(String ssid, String security, String level, String macAddress) {
        this.ssid = ssid;
        this.security = security;
        this.level = level;
        this.macAddress = macAddress;
    }

    public String getSsid() {
        return ssid;
    }

    public String getSecurity() {
        return security;
    }

    public String getLevel() {
        return level;
    }

    public String getMacAddress() {
        return macAddress;
    }

    private void writeObject(java.io.ObjectOutputStream out)
            throws IOException {
        out.writeObject(ssid);
        out.writeObject(security);
        out.writeObject(level);
        out.writeObject(macAddress);
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        this.ssid = (String) in.readObject();
        this.security = (String) in.readObject();
        this.level = (String) in.readObject();
        this.macAddress = (String) in.readObject();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Element element = (Element) o;

        if (ssid != null ? !ssid.equals(element.ssid) : element.ssid != null) return false;
        if (security != null ? !security.equals(element.security) : element.security != null)
            return false;
        if (level != null ? !level.equals(element.level) : element.level != null) return false;
        return macAddress != null ? macAddress.equals(element.macAddress) : element.macAddress == null;
    }

    @Override
    public int hashCode() {
        int result = ssid != null ? ssid.hashCode() : 0;
        result = 31 * result + (security != null ? security.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (macAddress != null ? macAddress.hashCode() : 0);
        return result;
    }
}