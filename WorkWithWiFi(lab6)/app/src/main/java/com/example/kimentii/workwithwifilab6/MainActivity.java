package com.example.kimentii.workwithwifilab6;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final int PERMISSION_REQUEST_CODE = 1;
    private final int NUM_OF_PERMISSIONS = 3;
    private final int REQUEST_PASSWORD = 4;

    public static String TAG = "TAG";
    private Element[] nets;
    private WifiManager wifiManager;
    private List<ScanResult> wifiList;
    private List<ScanResult> lastWifiList;
    private ListView netList;
    private Refresher mRefresher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        netList = (ListView) findViewById(R.id.listItem);
        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //detectWifi();

        netList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Element element = (Element) arg0.getAdapter().getItem(position);
                Intent intent = WiFiSettingsActivity.newIntent(getApplicationContext(), element);
                startActivity(intent);
            }
        });
        /*Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                System.out.println("Handler");
                detectWifi();
            }
        };
        mRefresher = new Refresher(handler);
        mRefresher.start();
        */
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                detectWifi();
                Snackbar.make(view, "Сканирование...", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });
        */

    }

    @Override
    protected void onPause() {
        super.onPause();
        mRefresher.setIsRepeat(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "ON puase");
        Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                System.out.println("Handler");
                detectWifi();
            }
        };
        mRefresher = new Refresher(handler);
        mRefresher.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "On result");
        if (requestCode == REQUEST_PASSWORD) {
            if (data != null) {
                //data.getIntExtra()
                Log.d(TAG, "your password");
            }
        }

        if (requestCode == PERMISSION_REQUEST_CODE) {
            Log.d(TAG, "Got permission");
        }
    }

    public void detectWifi() {
        //Toast.makeText(getApplicationContext(), "Scanning", Toast.LENGTH_SHORT).show();
        /* Для андроид выше 6.0
        if (checkCallingOrSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) ==
                PackageManager.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) ==
                        PackageManager.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_DENIED) {
            Log.d(TAG, "Asking permission.");
            requestPermissions(new String[]{
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CHANGE_WIFI_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
        */
        this.wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        this.wifiManager.startScan();
        this.wifiList = this.wifiManager.getScanResults();
        if (this.lastWifiList == null) {
            lastWifiList = wifiList;
        } else {
            if (lastWifiList.size() == wifiList.size()) {
                return;
            }
        }
        lastWifiList = wifiList;

        Log.d("TAG", wifiList.toString());
        this.nets = new Element[wifiList.size()];

        for (int i = 0; i < wifiList.size(); i++) {
            String item = wifiList.get(i).toString();
            String[] vector_item = item.split(", ");
            String item_ssid = vector_item[0];
            String item_mac = vector_item[1];
            String item_capabilities = vector_item[2];
            String item_level = vector_item[3];
            String ssid = item_ssid.split(": ")[1];
            String security = item_capabilities.split(": ")[1];
            String level = item_level.split(": ")[1];
            String mac = item_mac.split(": ")[1];
            nets[i] = new Element(ssid, security, level, mac);
        }

        AdapterElements listAdapter = new AdapterElements(this);
        netList.setAdapter(listAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length == NUM_OF_PERMISSIONS && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Get permission.");
                detectWifi();
            } else {
                Log.d(TAG, "Have no permission.:(");
                openApplicationSettings();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    public class AdapterElements extends ArrayAdapter<Object> {
        Activity context;

        public AdapterElements(Activity context) {
            super(context, R.layout.items, nets);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.items, null);

            TextView tvSsid = (TextView) item.findViewById(R.id.tvSSID);
            tvSsid.setText(nets[position].getSsid());

            TextView tvSecurity = (TextView) item.findViewById(R.id.tvSecurity);
            tvSecurity.setText(nets[position].getSecurity());

            TextView tvMacAddress = (TextView) item.findViewById(R.id.tvMacAddress);
            tvMacAddress.setText(nets[position].getMacAddress());

            TextView tvLevel = (TextView) item.findViewById(R.id.tvLevel);
            String level = nets[position].getLevel();

            try {
                int i = Integer.parseInt(level);
                if (i > -50) {
                    tvLevel.setText("Высокий");
                } else if (i <= -50 && i > -80) {
                    tvLevel.setText("Средний");
                } else if (i <= -80) {
                    tvLevel.setText("Низкий");
                }
            } catch (NumberFormatException e) {
                Log.d("TAG", "Неверный формат строки");
            }
            return item;
        }
    }
}