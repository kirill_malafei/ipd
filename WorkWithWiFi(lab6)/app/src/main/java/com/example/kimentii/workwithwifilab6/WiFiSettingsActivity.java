package com.example.kimentii.workwithwifilab6;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WiFiSettingsActivity extends AppCompatActivity {
    private static final String ELEMENT_EXTRA = "element extra";
    private final String TAG = "TAG";

    private TextView nameTextView;
    private TextView connectionStateTextView;
    private Button connectButton;
    private Button disconnectButton;
    private EditText passwordEditText;
    private Element element;

    public static Intent newIntent(Context packageContext, Element element) {
        Intent intent = new Intent(packageContext, WiFiSettingsActivity.class);
        intent.putExtra(ELEMENT_EXTRA, element);
        return intent;
    }

    private void initializeViews() {
        passwordEditText = (EditText) findViewById(R.id.password_edit_text);
        nameTextView = (TextView) findViewById(R.id.name_text_view);
        connectionStateTextView = (TextView) findViewById(R.id.connection_state_text_view);
        connectButton = (Button) findViewById(R.id.connect_button);
        disconnectButton = (Button) findViewById(R.id.disconnect_button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        element = (Element) getIntent().getSerializableExtra(ELEMENT_EXTRA);
        setContentView(R.layout.activit_wifi_settings);
        initializeViews();
        nameTextView.setText(element.getSsid());
        String wifiName = getWifiName(getApplicationContext());
        if (wifiName == null) {
            connectionStateTextView.setText("Not connected to WiFi network");
        } else if (wifiName.equals(element.getSsid())) {
            connectionStateTextView.setText("Connected to this network");
        } else {
            Log.d(TAG, "title: " + element.getSsid());
            Log.d(TAG, "Connected to: " + wifiName);
            connectionStateTextView.setText("Connected to other network");
        }
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WifiConfiguration wifiConfig = new WifiConfiguration();
                wifiConfig.SSID = String.format("\"%s\"", element.getSsid());
                String key = passwordEditText.getText().toString();
                wifiConfig.preSharedKey = String.format("\"%s\"", key);
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                // remember id
                int netId = wifiManager.addNetwork(wifiConfig);
                wifiManager.disconnect();
                wifiManager.enableNetwork(netId, true);
                wifiManager.reconnect();
                Toast toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
                /*if(){
                    toast.setText("Connected");
                    toast.show();
                }
                else{
                    toast.setText("Can't connect");
                    toast.show();
                }*/
            }
        });
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                wifiManager.disconnect();
            }
        });
        WiFiReceiver receiver = new WiFiReceiver(new Handler(), element, connectionStateTextView); // Create the receiver
        registerReceiver(receiver, new IntentFilter("android.net.wifi.STATE_CHANGE")); // Register receiver

    }

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    if (wifiInfo.getSSID().length() >= 2) {
                        return wifiInfo.getSSID().substring(1, wifiInfo.getSSID().length() - 1);
                    } else {
                        return wifiInfo.getSSID();
                    }
                }
            }
        }
        return null;
    }
}


